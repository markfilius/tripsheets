export const isServiceWorkerActive = async () => {
  if (!navigator.serviceWorker) return false

  return new Promise((resolve) => {
    navigator.serviceWorker.getRegistrations().then((swRegs) => {
      resolve(swRegs?.length && swRegs[0].active?.state === 'activated')
    })
  })
}
