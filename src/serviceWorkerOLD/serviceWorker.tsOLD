import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'
import { syncWithRemote } from '../common/syncWithRemote'
import { getLocation, isOnline, log } from '../common/utils'
import { db } from '../store/db'
import { setupListeners } from './serviceWorker.events'
import { createLocalBackup } from '../LocalBackup/LocalBackup.helper'

log('Service worker is starting')

cleanupOutdatedCaches()

// What does this do?
// @ts-ignore - WB_MANFIEST does not exist
precacheAndRoute(self.__WB_MANIFEST)

// Various listeners. incl app update
setupListeners()

// Find our permissions
navigator.permissions
  .query({
    // @ts-ignore - that name is not in the ts definitions
    name: 'periodic-background-sync',
  })
  .then((status) =>
    log(
      `Have ${
        status.state === 'granted' ? '' : 'NOT GOT'
      } permissions for Periodic Sync`
    )
  )
navigator.permissions
  .query({ name: 'notifications' })
  .then((status) =>
    log(
      `Have ${
        status.state === 'granted' ? '' : 'NOT GOT'
      } permissions for Notifications`
    )
  )
// @ts-ignore - doesn't know setAppBadge
// if (navigator.setAppBadge) navigator.setAppBadge(42) // Seems only to work from UI thread - that wasnt the idea!

// Main loop - look every x seconds if we're online, sync and backup
setInterval(async () => {
  let hasLocation = false

  // Get location
  const pos = await getLocation()
  if (pos.slice(0, 3) === 'LON') hasLocation = true

  // Get online status
  const onlineStatusNav = isOnline()

  // Sync
  if (onlineStatusNav) {
    try {
      await syncWithRemote()
    } catch (e) {
      log(
        'syncWithRemote in service worker failed - likely the db has closed',
        true
      )
      console.log('syncWithRemote in service worker failed with:', e)
      // @ts-ignore - e is unknown
      if (e.name === 'DatabaseClosedError') {
        log('Will re-open the db...')
        db.open()
      }
    }
  } else {
    log(`ServiceWorker: noSync${hasLocation ? ', pos' : ''}`)
  }

  // Backups
  await createLocalBackup('walks')
  await createLocalBackup('users')
  await createLocalBackup('plbs')
  await createLocalBackup('messages')
}, 30_000)
