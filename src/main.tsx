import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { swRegister } from '../serviceWorker/swRegister.mjs'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)

// Register the service worker
swRegister()
