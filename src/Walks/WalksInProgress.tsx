import {
  Box,
  Button,
  HStack,
  IconButton,
  StackDivider,
  Text,
  useToast,
  VStack,
} from '@chakra-ui/react'
import { AddIcon, CheckIcon, InfoOutlineIcon } from '@chakra-ui/icons'
import { useEffect, useState } from 'react'
import { MakeWalk } from './MakeWalk'
import { useLiveQuery } from 'dexie-react-hooks'
import { db, Walk } from '../store/db'
import { palette } from '../config'
import { registerFor, startWalk } from './walks.model'
import { WalkInfo } from './WalkInfo'
import { log } from '../common/utils'
import { syncWithRemote } from '../common/syncWithRemote'
import { MyWalks } from './MyWalks'
import { getBadgeFor } from './Walks.helpers'
import { WalkHistory } from './WalkHistory'

export const WalksInProgress = () => {
  const toast = useToast()

  const appStatusDexie = useLiveQuery(() => db.appStatus.toCollection().last())
  const walksDexie = useLiveQuery(() => db.walks.toArray())

  const [myWalks, setMyWalks] = useState<Walk[]>([])
  const [haveDraftWalk, setHaveDraftWalk] = useState(false)
  const [amLeading, setAmLeading] = useState(false)
  const [openWalks, setOpenWalks] = useState<Walk[]>([])
  const [startedWalks, setStartedWalks] = useState<Walk[]>([])
  const [endedWalks, setEndedWalks] = useState<Walk[]>([])
  const [showMakeWalkIsOpen, setShowMakeWalkIsOpen] = useState(false)
  const [showWalkHistoryIsOpen, setShowWalkHistoryIsOpen] = useState(false)
  const [showWalkInfoFor, setShowWalkInfoFor] = useState('')
  const [editWalkId, setEditWalkId] = useState('')

  // Effect to get the latest walks per status
  useEffect(() => {
    if (!appStatusDexie?.userId) return
    if (!walksDexie) return

    // Find all walks started
    const startedWalksDexie =
      walksDexie?.filter((w) => w.status === 'started') || []
    setStartedWalks(startedWalksDexie)
  }, [walksDexie, appStatusDexie?.userId])

  return (
    <>
      <Box className={'userForm'} mt={8}>
        <VStack divider={<StackDivider borderColor="gray.200" />}>
          {/* Walks in Progress */}
          {startedWalks.length && (
            <Box className={'listItemContainer'}>
              <Box className={'registerWalksList'}>
                <>
                  <Text as={'b'}>Walks in progress:</Text>
                  {startedWalks.map((w: Walk) => (
                    <Box
                      key={'reg-' + w.walkId}
                      className={'registerWalksItem'}
                    >
                      <Box display={'flex'} alignItems={'center'}>
                        <IconButton
                          icon={<InfoOutlineIcon />}
                          size={'sm'}
                          mr={2}
                          onClick={() => setShowWalkInfoFor(w.walkId)}
                          aria-label={'Info about the walk'}
                        />
                        <Box mr={2}>{w.title}</Box>
                        {getBadgeFor(w, appStatusDexie)}
                      </Box>
                    </Box>
                  ))}
                </>
              </Box>
            </Box>
          )}
        </VStack>
      </Box>

      {/* Walk info panel */}
      {showWalkInfoFor && (
        <WalkInfo
          walkId={showWalkInfoFor}
          onClick={() => setShowWalkInfoFor('')}
        />
      )}
    </>
  )
}
