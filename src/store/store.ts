export interface LogItem {
  id: string
  repeat: number
  time: string
  data: any
}
