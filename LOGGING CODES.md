Logging mesages
===============

## Sync issues
* ESYNMUL nrSyncs 2remoteIds table    Multiple syncs at same time
* WSYNRETRY nrUserIds                 Multiple syncs at same time NumberOfUsers
* ESYNMIS table userId                Sync data missing for a table
* ESYNMIS2 table:remoteIds userId     This table has these remote ids remotely
* ESYNMIS3 table nrSyncers            The number of persons syncing right now
* ESYNMIS4 backupSignature            Signature of each walks backup
* EMULREM table nrFiles userId        Multiple Remote files different
* EMULSAM table nrFiles userId        Multiple Remote files the same
* EPUSH table message userId          PushToRemote for table failed
* WMULSYN remoteIds userId            Multiple syncs detected when trying to syncLock for me
* WSWMIS userId                       ServiceWorker wasn't found 10 sec after app start

## Verify all is well before sync
* EVERMIS table userId                     VerifyMissing table
* WVERREST table userId                    Verify has autorestored this table
* WVERFIX table diff local/remote userId   VerifyAutoFixed table LengthDiffExclDeleted-LocalLessThanRemote Local/Remote-LengthsInclDeleted
* WVERLOCAL table diff local/remote userId Verify table LengthDiffExclDeleted-LocalMoreThanRemote Local/Remote-LengthsInclDeleted
* WPUSHBROK table userId                   Push for this table was broken off
* SVERFOU table age signature              Found a backup for table, age hours ago, signature
* SVERREST table userId                    Successfully restored table during verify

## Sanity = Verify all went well during after sync
*  ESANAM table diff local/remote      Sanity AfterMerge LengthDiffExclDeleted LengthsInclDeleted
*  ESANBP table REMMIS                 Sanity BeforePush Remote missing
*  ESANBP table diff local/remote      Sanity BeforePush LengthDiffExclDeleted LengthsInclDeleted
*  WSANBP table REGFAIL                Sanity BeforePush registration failed
*  ESANAPA table diff local/remote     Sanity AfterPush Again LengthDiffExclDeleted LengthsInclDeleted
*  ESANAP table REGFAIL                Sanity AfterPush registration failed

## General Messages
*  MSGNEW                              Message newMsgMade
*  MSG action content                  Message action content
*  MSGWEND title                       Message walkEnded title
