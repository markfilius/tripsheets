toEnv=$1
if [ "$toEnv" != "l" ] && [ "$toEnv" != "d" ] && [ "$toEnv" != "c" ] && [ "$toEnv" != "lb" ]; then
  echo '--- Provide a target: l(ocal), d(emo), c(bc) or lb(bw)'
  exit
fi

bold=$(tput bold)
normal=$(tput sgr0)

echo '---'
echo "--- Make sure any local SERVER is ${bold}STOPPED${normal}!"
echo '---'
read -p '--- Is it stopped [Y] ' do
if [ "$do" != "" ] && [ "$do" != "y" ]; then
  echo '--- Stop the local server first'
  exit
fi

target=''
branch=''
if [ "$toEnv" == "l" ]; then
  target='local';
  branch='master'
fi
if [ "$toEnv" == "d" ]; then
  target='demo';
  branch='release-demo'
fi
if [ "$toEnv" == "c" ]; then
  target='cbc';
  branch='release-cbc'
fi
if [ "$toEnv" == "lb" ]; then
  target='lbbw';
  branch='release-lbbw'
fi

# Where are we now ?
currentBranch=`git rev-parse --abbrev-ref HEAD`
if [ $? -ne 0 ]; then exit; fi;

# To the branch
git switch $branch
result=$?
if [ $result -eq 128 ]; then
  echo "--- Creating branch "$branch
  git switch -c $branch
elif [ $result -ne 0 ]; then
  echo "--- ${bold}Cant switch${normal} to "$branch
  echo "--- Stopping"
  exit
fi
echo "--- Switched from ${bold}${currentBranch}${normal} to ${bold}"$branch$normal

git rebase $currentBranch
if [ $? -ne 0 ]; then
  echo '--- Rebase off '$currentBranch' is incorrect. Aborting.';
  exit;
fi

# get correct env file
cp .env-$target .env
if [ $? -ne 0 ]; then
  echo '--- Cant get correct env file. Aborting.';
  exit;
fi

if [ "$target" == "local" ]; then exit; fi

# Build
echo '--- Building...'
rm -rf dist
rm -rf public
npm run build
if [ $? -ne 0 ]; then
  echo "--- Build  ${bold}failed${normal}. Still in release branch!";
  git status
  exit
fi
echo '--- Copying images...'
cp -a images${target}/. dist/images
swFile=serviceWorker/swFunctions.mjs
if [ -f $swFile ]; then
  echo '--- Copying specific service worker registration & manifest...'
  cp $swFile dist/
#  cp src/serviceWorker/swRegister.mjs dist/swRegister.mjs
#  cp src/serviceWorker/test2.js dist/test2.js
  cp serviceWorker/webManifest.json dist/webManifest.json
fi
cp -a dist/. public/
echo "--- Files in public folder:"
ls -al public/

# Deploy
firebase deploy --only hosting:$target
if [ $? -ne 0 ]; then
  echo "--- Deploy seems ${bold}wrong${normal}. Switch back to "$currentBranch;
  git switch $currentBranch
  cp  .env-local .env
  exit
fi

# Back to start point
git switch $currentBranch
cp .env-local .env

echo '---'
echo "--- Deployed to ${bold}"$target$normal
echo '--- Now in this environment:'
cat .env | grep ENVIRONMENT
