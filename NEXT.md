

# firebase hosting:
    this error:  firebase target:apply hosting demo <resources...>
    try like this:
        firebase target:apply hosting demo tripsheets-bd1e4
        firebase target:apply hosting lbbw lbbw-tripsheets
    and see the list with
        firebase target
        
# TO DO list
## bugs
* search in walk history and user is sooo sloooow
* proper use of palette colours:
    * always use the correct one, e.g. see walkhistory#178 is always light palette

## enhancements
* check for updates
* use space next to help icon for messges and action buttons
   * area next to help btn: use that for messages and extra btn like reload if needed
* edit of completed walks for leaders
* correct auth on the firebase function calls (see permissions in the function)
* animation/slide for main menu
* sw cache expiry time
   * https://gomakethings.com/how-to-set-an-expiration-date-for-items-in-a-service-worker-cache/

## force the new version  
    On all devices where you use the app, so all phones, tablets and computers
    
    Step 1: 
        very important! close all browser tabs that are open. Even a single (maybe very old) tab will prevent the update
        'how' to close depends on your phone and browser, but this may help:
            Android: 	https://ccm.net/apps-sites/web/941-how-to-close-all-tabs-in-google-chrome-for-android/
            iOS: 	https://support.apple.com/en-gb/guide/iphone/iph489b9313f/ios 
    
    Step 2 for Android only:
        If you have an icon on the homescreen or somewhere, long press on it to Uninstall
        
    Step 3:
        restart the phone. It isn't really necessary, but sometimes it can help
    
    Step 4:
        reinstall by - in your browser - going to https://lbbw-tripsheets.com/install
        then Register > Yes > Select youself

## future ideas

* replace dexie with other indexeddb wrapper, OR, CRDT: https://crdt.tech/implementations
* max nr on a walk + wait list for full
   * how to confirm?
   * for exptected too?
* repeat btn shows previous start and end times
   * try this: search and repeat: the window is too small
* links as a separate list in the walkInfo
   * link in and message and walk notes: 
   * http or www. or [LINK,text,url]
   * see how tips does it
* add dark mode button
* keep current user in localstorage for reinstall.
   * but: how to recover after impersonation??? on impersonation: remove the localstorage???
   * EndMe btn next to name
   * impersonateBy field in appStatus
* dad jokes on swipe up
* one leader can add/remove another leader
* get perSync working! sync from sw
* day after walk -> auto draft
* reply to a message
* more than 1 draft walk - not so easy
* basic medical info
* better analytics
* remove ESYNMIS2,3,4
*  when delete a file, check if not in use anywhere else
*  prune the db
*  sync on unmount of app (already syncs on visibility changes)
*  battery level
*  move completed walks to different table
*  https://million.dev/docs/introduction - speed linter


## notes on various functions

* periodicSync - not on safari - not firing in chrome either :-(
    * on help request, get location and send message if have internet
    * if internet -> sync
    * https://developer.mozilla.org/en-US/docs/Web/API/Web_Periodic_Background_Synchronization_API

* map
    * show me and history
    * with requestHelp function, ping until have internet
    * show a map of start/end/current gps point
    * function with POI on request
    * with tracking in theory, yes, but in reality will never work as PWA
    * https://github.com/RichardMaher/Brotkrumen
    * https://stackoverflow.com/questions/72422115/background-geolocation-tracking-using-service-worker-for-web-app

* documents
    * add a document library for membership, claims, pack list, etc

* other club - Cairns
    * change name in manifest
    * legal info per club (now on lbbw website)
    * $300
