
# TRIPSHEETS APP

Built using React with Vite

Hosting on Firebase

Separate app (tripsheets-functions) as backend with 1 function only: write to S3


## Commands
* run locally

`$ npm run start`

* make a release with version number

` $ npm run release`

* deploy to environment
* a server must ron locally at the same time!

```
$ npm run demo
$ npm run lbbw
# npm run cdc
```

* create a local backup, so including all files
* backups through the app is dexie only

`$ npm run backup`
