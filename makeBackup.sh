toEnv=$1
if [ "$toEnv" != "l" ] && [ "$toEnv" != "d" ] && [ "$toEnv" != "c" ] && [ "$toEnv" != "lb" ]; then
  echo '--- Provide a source: l(ocal), d(emo), c(bc) or lb(bw)'
  exit
fi

source=''
if [ "$toEnv" == "l" ]; then
  source='local';
fi
if [ "$toEnv" == "d" ]; then
  source='demo';
fi
if [ "$toEnv" == "lb" ]; then
  source='lbbw';
fi
if [ "$toEnv" == "c" ]; then
  source='cbc';
fi

# Make the folder
timestamp=$(date +%Y%m%d-%H%M%S)
newFolder="backups/$source/$timestamp"
mkdir -p $newFolder

# Copy the files
AWS_PROFILE=markfilius aws s3 cp s3://lbbw-tripsheets/$source $newFolder --recursive
if [ $? -ne 0 ]; then
  echo '---'
  echo '--- Something went wrong'
  exit;
fi

echo '---'
echo '--- Result folders/files'
echo $newFolder
ls -alR $newFolder
