/// <reference lib="webworker" />

const CACHE_NAME = `lbbw-tripsheets`

// For offline use, will be placed in the cache
const APP_STATIC_ASSETS = [
  '/',
  '/index.html',
  '/webManifest.json',
  '/assets/index.js', // Surprisingly, this will also cache the chunks with hashes
  '/assets/index.css',
]
const APP_STATIC_IMAGES = [
  '/images/clubLogo.ico',
  '/images/clubLogo-192x192.png',
  '/images/clubLogo-512x512.png',
  '/images/help-user.png',
  '/images/help-walks.png',
]

// On install (or update of the sw) fill the cache
self.addEventListener('install', (event) => {
  event.waitUntil(
    (async () => {
      const cache = await caches.open(CACHE_NAME)
      await cache.addAll(APP_STATIC_ASSETS.concat(APP_STATIC_IMAGES))
    })()
  )
})

// On activate (of the updated sw), delete all old caches and assign the client
self.addEventListener('activate', (event) => {
  console.log('activate event fired', event)
  event.waitUntil(
    (async () => {
      const names = await caches.keys()
      console.log('names of caches on activate', names)
      await Promise.all(
        names.map((name) => {
          if (name !== CACHE_NAME) {
            return caches.delete(name)
          }
        })
      )

      // Assign the client to this sw
      await self.clients.claim()

      // Checking if all good
      // const matchResult = await self.clients.matchAll()
      // console.log('matchresult', matchResult)
      // const cacheKeysResult = await caches.keys()
      // console.log('cacheKeysResult', cacheKeysResult)
      // const open = await caches.open(cacheKeysResult[0])
      // const cacheResponsesResult = await open.matchAll()
      // console.log('cacheResponsesResult', cacheResponsesResult)
    })()
  )
})

// Respond to network requests
self.addEventListener('fetch', (event) => {
  // Force SPA behaviour
  // if (event.request.mode === "navigate") {
  //   // Return to the index.html page
  //   event.respondWith(caches.match("/"));
  //   return;
  // }

  // Respond from network first
  event.respondWith(
    (async () => {
      const cache = await caches.open(CACHE_NAME)

      // Get from network
      try {
        if (navigator.onLine) {
          const response = await fetch(event.request)
          if (response.type === 'basic') {
            // Cache the normal requests (so not cors or opaque)
            await cache.put(event.request, response.clone())
          }
          return response
        }
      } catch (error) {
        // Something went wrong, likely we went offline
      }

      // We're offline, so use the cache
      const cachedResponse = await cache.match(event.request.url)
      if (cachedResponse) {
        return cachedResponse
      }

      // No response available
      // Note: cant return 204 with null body, it crashes the fb function
      return new Response(undefined, { status: 404 })
    })()
  )
})

// setInterval(async () => {
//   console.log('in the 30 sec loop')
//   // console.log('creating backup from test2.js')
//   // createLocalBackup('walks')
// }, 30_000)
