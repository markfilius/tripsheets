import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
// @ts-ignore
// import packageJson from './package.json'
// const version = packageJson.version

// https://vitejs.dev/config/
export default defineConfig({
  define: { global: 'window' }, // Because Vite doesnt create 'global' by default
  base: '/',
  plugins: [react()],
  build: {
    minify: true,
    sourcemap: true,
    copyPublicDir: false,
    // rollupOptions: {
    //   output: {
    //     assetFileNames: `assets/[name][extname]`,
    //     chunkFileNames: `[name].js`,
    //   },
    // },
  },
})
